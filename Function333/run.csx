#r "Newtonsoft.Json"
#r "SendGrid"

using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SendGrid.Helpers.Mail;
using RestSharp;

public class Ask
{
    public double Price { get; set; }

    public double Volume { get; set; }

    public string TimeStamp { get; set; }
}

public static void Run(TimerInfo myTimer, TraceWriter log, out Mail message)
{
    message = null;
    log.Info($"C# Timer trigger function executed at: {DateTime.Now}");


    var client = new RestClient("https://api.kraken.com");

    var request = new RestRequest("0/public/Depth?pair=LTCEUR", Method.POST);

    IRestResponse response = client.Execute(request);

    dynamic parsedResponse = JObject.Parse(response.Content);

    var asks = new List<Ask>();

    foreach (var trade in parsedResponse.result.XLTCZEUR.asks)
    {
        asks.Add(new Ask { Price = trade[0], Volume = trade[1], TimeStamp = trade[2] });
    }

    var lowestBuyOffer = asks.OrderBy(a => a.Price).FirstOrDefault();

    log.Info($"The lowest buy offer: {lowestBuyOffer?.Price}, volume: {lowestBuyOffer?.Volume}");

    if (lowestBuyOffer?.Price > 15.00)
    {
        message = new Mail();

        var personalization = new Personalization();
        personalization.AddTo(new Email("bandister@gmail.com"));
        message.AddPersonalization(personalization);

        var messageContent = new Content("text/html", $"The lowest buy offer: {lowestBuyOffer?.Price}, volume: {lowestBuyOffer?.Volume}");
        message.AddContent(messageContent);
        message.Subject = "You have to buy LTC";
        message.From = new Email("abremer@future-processing.com");
    }


}